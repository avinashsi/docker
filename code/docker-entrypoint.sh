#!/bin/bash
set -e

chown -R 888.888 /opt/fitnesse
exec gosu fitnesse java -jar /opt/fitnesse/FitNesseRoot/lib/fitnesse-standalone.jar  -p 2000 -r /opt/fitnesse/FitNesseRoot/FitNesseRoot "$@"
